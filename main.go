package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	// environment variables
	EnvDbHost     = "DATABASE_HOST"
	EnvDbPort     = "DATABASE_PORT"
	EnvDbUsername = "DATABASE_USERNAME"
	EnvDbPassword = "DATABASE_PASSWORD"
	EnvDbName     = "DATABASE_NAME"
	EnvCmcApiKey  = "CMC_API_KEY"
)

func main() {
	// env
	dbHost := os.Getenv(EnvDbHost)
	dbPort := os.Getenv(EnvDbPort)
	dbUsername := os.Getenv(EnvDbUsername)
	dbPassword := os.Getenv(EnvDbPassword)
	dbName := os.Getenv(EnvDbName)
	cmcApiKey := os.Getenv(EnvCmcApiKey)

	if dbHost == "" {
		log.Fatalf("%s env variable is required", EnvDbHost)
	}
	if dbPort == "" {
		log.Fatalf("%s env variable is required", EnvDbPort)
	}
	if dbUsername == "" {
		log.Fatalf("%s env variable is required", EnvDbUsername)
	}
	if dbPassword == "" {
		log.Fatalf("%s env variable is required", EnvDbPassword)
	}
	if dbName == "" {
		log.Fatalf("%s env variable is required", EnvDbName)
	}
	if cmcApiKey == "" {
		log.Fatalf("%s env variable is required", EnvCmcApiKey)
	}

	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		dbHost, dbPort, dbUsername, dbPassword, dbName)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalf("error connecting to DB: %v", err)
	}
	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)

	if err = db.Ping(); err != nil {
		log.Fatalf("error connecting to DB: %v", err)
	}

	log.Printf("dopytujem coinmarketcap.com")
	cmcListing := getCmcPrices(cmcApiKey)
	log.Printf("ukladam do DB")
	savePrices(cmcListing, db)
	log.Printf("koncim")
}

func getCmcPrices(cmcApiKey string) *CmcListing {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest", nil)
	if err != nil {
		log.Printf("error creating request: %v", err)
		return nil
	}

	q := url.Values{}
	q.Add("start", "1")
	q.Add("limit", "5000")
	q.Add("convert", "USD")

	req.Header.Set("Accepts", "application/json")
	req.Header.Add("X-CMC_PRO_API_KEY", cmcApiKey)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("error sending request to server: %v", err)
		return nil
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		log.Printf("error reading request: %v", err)
		return nil
	}
	var result CmcListing
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		log.Printf("error unmarshalling: %v", err)
		return nil
	}
	return &result
}

func savePrices(cmcListing *CmcListing, db *sql.DB) {
	for _, data := range cmcListing.Data {
		_, err := db.Exec(`INSERT INTO prices (name, symbol, price, volume_24, market_cap, time) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (symbol, time) DO NOTHING`,
			data.Name, data.Symbol, data.Quote.USD.Price, data.Quote.USD.Volume24H, data.Quote.USD.MarketCap, data.Quote.USD.LastUpdated)
		if err != nil {
			log.Printf("error inserting into DB: %v", err)
			return
		}
	}
}

type CmcListing struct {
	Data []struct {
		Name   string `json:"name"`
		Symbol string `json:"symbol"`
		Quote  struct {
			USD struct {
				Price       float64   `json:"price"`
				Volume24H   float64   `json:"volume_24h"`
				MarketCap   float64   `json:"market_cap"`
				LastUpdated time.Time `json:"last_updated"`
			} `json:"USD"`
		} `json:"quote"`
	} `json:"data"`
}
