FROM golang:1.17 AS builder
WORKDIR /opt/build/
COPY . .
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o app .

FROM alpine:3.14

WORKDIR /opt/app/
COPY --from=builder /opt/build/app .
CMD ["./app"]